var smartgrid = require('smart-grid');

var settings = {
    outputStyle: 'scss', /* less || scss || sass || styl */
    columns: 12, /* number of grid columns */
    offset: '20px', /* gutter width px || % */
    mobileFirst: false, /* mobileFirst ? 'min-width' : 'max-width' */
    oldSizeStyle: false,
    container: {
        maxWidth: '945px', /* max-width оn very large screen */
        fields: '60px' /* side fields */
    },
    breakPoints: {
        lg: {
            width: '1200px', /* -> @media (max-width: 1100px) */
        },
        md: {
            width: '960px'
        },
        sm: {
            width: '780px',
            fields: '10px' /* set fields only if you want to change container.fields */
        },
        xs: {
            width: '560px'
        }
    }
};

smartgrid('./src/scss', settings);